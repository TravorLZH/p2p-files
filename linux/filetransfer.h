#ifndef	FILETRANSFER_H
#define	FILETRANSFER_H

#define	MAX_BASENAME	0x80
#define	FT_PORT	2333

struct fileheader{
	char filename[MAX_BASENAME];
	unsigned int size;	// Size of file in network byte order
};

/** This function checks if the server accepts this header */
extern int check_server(int sock,struct fileheader *header);

/** This function checks if the server completes the request */
extern int check_complete(int sock);

/** Allow the client to send files */
extern void begin_transfer(int sock);

/** Tell the client to close the connection */
extern void complete_transfer(int sock);

/** Fill up the fileheader structure */
extern void fill_fileheader(struct fileheader*,const char*,unsigned int);

/** Dump the fileheader structure
 * @return	Size of file in host byte order
 */
extern unsigned int dump_fileheader(struct fileheader*);

#endif
