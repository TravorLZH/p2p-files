#include <sys/socket.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include "filetransfer.h"

char buf[32];
const char proper_response[]="Que tal";
const char complete_response[]="Adios";
struct timeval timeout={20,0};	// 20 seconds

int check_server(int sock,struct fileheader *header)
{
	int flags;
	int ret;
	fd_set set;
	FD_ZERO(&set);
	FD_SET(sock,&set);

	send(sock,header,sizeof(*header),0);
	memset(buf,0,sizeof(buf));
	if((ret=select(sock+1,&set,NULL,NULL,&timeout))<0){
		return -1;
	}else if(ret==0){
		errno=ETIMEDOUT;
		return -1;
	}else{
		if(FD_ISSET(sock,&set)){
			recv(sock,buf,sizeof(buf),0);
			goto done;
		}
	}
done:
	return strcmp(buf,proper_response);
}

void begin_transfer(int sock)
{
	send(sock,proper_response,sizeof(proper_response),0);
}

int check_complete(int sock)
{
	memset(buf,0,sizeof(buf));
	recv(sock,buf,sizeof(buf),0);
	return strcmp(buf,complete_response);
}

void complete_transfer(int sock)
{
	send(sock,complete_response,sizeof(complete_response),0);
}

void fill_fileheader(struct fileheader *header,const char *filename,
		unsigned int size)
{
	assert(header!=NULL);
	assert(filename!=NULL);
	strncpy(header->filename,filename,sizeof(header->filename));
	header->size=htonl(size);
}

unsigned int dump_fileheader(struct fileheader *header)
{
	return (unsigned int)ntohl(header->size);
}
