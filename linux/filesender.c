#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <libgen.h>
#include "filetransfer.h"

struct fileheader header;
FILE *file=NULL;
int sock=0;
char *address=NULL;
unsigned int file_size;

void make_connection(void)
{
	sock=socket(AF_INET,SOCK_STREAM,0);
	struct sockaddr_in addr;
	memset(&addr,0,sizeof(addr));
	addr.sin_family=AF_INET;
	addr.sin_addr.s_addr=inet_addr(address);
	addr.sin_port=htons(FT_PORT);
	connect(sock,(struct sockaddr*)&addr,sizeof(addr));
}

void send_file(void)
{
	assert(sock!=0);
	assert(file!=NULL);
	// Send the content of the file
	char *content=(char*)malloc(file_size+1);
	memset(content,0,file_size+1);
	fread(content,1,file_size,file);
	send(sock,content,file_size,0);
	check_complete(sock);	// Check if the server received everything
	close(sock);	// We don't need socket now
	free(content);
}

int main(int argc,char **argv)
{
	if(argc<3){
		fprintf(stderr,"Usage: %s [file] [ip address]\n",argv[0]);
		return 1;
	}
	if((file=fopen(argv[1],"rb"))==NULL){
		perror(argv[1]);
		return 1;
	}
	address=argv[2];	// Where to receive this file
	// Get the length of file
	fseek(file,0,SEEK_END);
	file_size=ftell(file);
	rewind(file);
	fill_fileheader(&header,basename(argv[1]),file_size);
	printf("%s: %s => %s\n",argv[0],header.filename,address);
	make_connection();
	if(check_server(sock,&header)){
		fprintf(stderr,"%s: Not a file receiver\n",argv[2]);
		close(sock);
		fclose(file);
		return 1;
	}
	send_file();
	printf("%s: %s: SUCCESS\n",argv[0],header.filename);
	fclose(file);
	return 0;
}
