#ifndef	DEBUG_H
#define	DEBUG_H
#ifndef	NDEBUG
#define	trace(...)	printf(__VA_ARGS__)
#else
#define	trace(...)	do{}while(0)
#endif
#endif
