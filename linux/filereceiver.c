#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <ifaddrs.h>
#include <assert.h>
#include "filetransfer.h"
#include "debug.h"

struct fileheader header;
struct sockaddr_in client_addr;
int sock=0;
FILE *file=NULL;
unsigned int file_size;
char ip[INET_ADDRSTRLEN];
char path[MAX_BASENAME+3];

char *get_host_address(void)
{
	char hostname[128];
	struct hostent *hent;
	gethostname(hostname,sizeof(hostname));
	hent=gethostbyname(hostname);
	return inet_ntoa(*(struct in_addr*)(hent->h_addr_list[0]));
}

char *get_ip_address(void)
{
	int s=socket(AF_INET,SOCK_DGRAM,0);
	struct sockaddr_in a;
	char *ret=NULL;
	socklen_t len=sizeof(a);
	memset(&a,0,len);
	a.sin_family=AF_INET;
	a.sin_addr.s_addr=inet_addr("8.8.8.8");
	a.sin_port=80;
	connect(s,(struct sockaddr*)&a,len);
	getsockname(s,(struct sockaddr*)&a,&len);
	ret=inet_ntoa(a.sin_addr);
	close(s);
	return ret;
}

void start_server(void)
{
	sock=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	struct sockaddr_in my_addr;
	memset(&my_addr,0,sizeof(my_addr));
	my_addr.sin_family=AF_INET;
	my_addr.sin_addr.s_addr=htonl(INADDR_ANY);
	my_addr.sin_port=htons(FT_PORT);
	bind(sock,(struct sockaddr*)&my_addr,sizeof(my_addr));
	listen(sock,20);
}

int found_request(struct fileheader *header)
{
	int request;
	int offset=0;
	socklen_t addr_sz=sizeof(struct sockaddr_in);
	request=accept(sock,(struct sockaddr*)&client_addr,&addr_sz);
	assert(addr_sz==sizeof(struct sockaddr_in));
	while(offset<sizeof(*header)){
		offset+=recv(request,(char*)header+offset,
				sizeof(*header)-offset,0);
	}
	return request;
}

int read_file(int request,struct fileheader *header,char *buf)
{
	unsigned int size=dump_fileheader(header);
	unsigned int read=0;
	unsigned int tmp=0;
	begin_transfer(request);
	while(read<size){
		tmp=recv(request,buf+read,size-read,0);
		if(tmp<=0){
			return -1;
		}
		read+=tmp;
	}
	return read;
}

int main(int argc,char **argv)
{
	printf("%s: IP Address: %s\n",argv[0],get_ip_address());
	start_server();
	int request=found_request(&header);
	file_size=dump_fileheader(&header);
	inet_ntop(AF_INET,&client_addr.sin_addr,ip,INET_ADDRSTRLEN);
	printf("%s: %s (%dB) [y]",ip,header.filename,file_size);
	switch(getchar()){
	case 'y':case 'Y':case '\n':
		break;
	default:
		// Send anything other than 'Que tal' means to refuse
		printf("Declined\n");
		send(sock,"lalala",6,0);
		close(sock);
		close(request);
		return 1;
	}
	char *content=(char*)malloc(file_size);
	memset(content,0,file_size);
	if(read_file(request,&header,content)!=file_size){
		perror("error");
		close(sock);
		close(request);
		return -1;
	}
	close(sock);
	close(request);
	sprintf(path,"./%s",header.filename);
	printf("%s: %s => %s\n",argv[0],header.filename,path);
	if((file=fopen(path,"wb"))==NULL){
		perror(path);
		return 1;
	}
	fwrite(content,1,file_size,file);
	free(content);
	printf("%s: %s: SUCCESS\n",argv[0],path);
	fclose(file);
	return 0;
}
