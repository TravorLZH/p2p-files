import logging
import sqlite3
import sys

connect = sqlite3.connect('../p2p_data.db')
cursor = connect.cursor()
cursor.execute("""CREATE TABLE IF NOT EXISTS Addresses
    (ID INT PRIMARY KEY NOT NULL,
     NAME          TEXT NOT NULL,
     TAG           TEXT,
     IP            TEXT NOT NULL);""")

_c = cursor.execute("""SELECT name FROM sqlite_master WHERE type='table' AND name='Settings';""")

if not _c:
    cursor.execute("""CREATE TABLE Settings
        (ID INT PRIMARY KEY NOT NULL,
        NAME          TEXT NOT NULL,
        DATA           TEXT);""")
    cursor.execute("""INSERT into Settings (id, name, data)\
        VALUES (0, 'file_save_path', 'C:/Users/Yirou Wang/Downloads/""")
connect.commit()

logger = logging.getLogger('P2P.TableExecutor')
logger.setLevel(logging.DEBUG)


def get_ip_choices():
    c = cursor.execute("""SELECT NAME, IP FROM Addresses""")
    ips = {}
    for r in c:
        ips[r[0]] = r[1]
    logger.debug('Name choices are returned.')
    return ips


def get_ip_data(name):
    if name_exists(name):
        c = cursor.execute("""SELECT NAME, TAG, IP FROM Addresses WHERE name='{}'""".format(name))
        for r in c:
            l = []
            for data in r:
                if data == None:
                    l.append('')
                elif data == 'None':
                    l.append('None')
                else:
                    l.append(data)
            logger.debug("{} is found for name query {}.".format(l, name))
            return l
    else:
        return ('', '', '')


def get_max_id():
    c = cursor.execute("""SELECT max(id) FROM Addresses""")
    for r in c:
        logger.debug('Max ID {} returned.'.format(r[0]))
        if r[0]: return r[0]
        else: return 0


def name_exists(name):
    c = cursor.execute("""SELECT EXISTS(SELECT 1 FROM Addresses WHERE name="{}")""".format(name))
    for r in c:
        return r[0]


def add_ip(name, ip, *tag):
    if tag:
        cursor.execute("""INSERT INTO Addresses (id, name, tag, ip)\
         VALUES ({id},
                '{name}',
                '{tag}',
                '{ip}')""".format(id=int(get_max_id() + 1),
                                  name=str(name),
                                  tag=str(tag[0]),
                                  ip=str(ip)))
    else:
        cursor.execute("""INSERT INTO Addresses (id, name, ip)\
        VALUES ({id},
                '{name}',
                '{ip}')""".format(id=int(get_max_id() + 1),
                                  name=str(name),
                                  tag='',
                                  ip=str(ip)))
    connect.commit()
    logger.info('IP {}({}) added into database.'.format(ip, name))


def edit_ip(name, changed_things: dict):
    for name, data in changed_things.items():
        cursor.execute("""UPDATE Addresses SET {name}='{data}' WHERE name='{oriname}'""".format(
            data=data, name=name, oriname=name
        ))
        logger.debug('{0} of IP {1} was updated to {2}'.format(name, data, name))
    connect.commit()
    logger.info('IP {0} was updated: {1}'.format(name, changed_things))


def delete_ip(name):
    cursor.execute("""DELETE FROM Addresses WHERE name='{}'""".format(name))
    connect.commit()
    logger.info('IP {} deleted from table Addresses.'.format(name))


def get_settings():
    settings = {}
    c = cursor.execute("""SELECT NAME, DATA FROM Settings""")
    for r in c:
        settings[r[0]] = r[1]
    logger.debug('Settings "{}" are returned.'.format(settings))
    return settings


def save_settings(settings):
    for name, data in settings.items():
        cursor.execute("""UPDATE Settings SET DATA='{data}' WHERE NAME='{name}'""".format(data=data, name=name))
    connect.commit()
    logger.debug('Settings {} saved into database.'.format(settings))


def close_connection():
    connect.close()
    sys.exit()
