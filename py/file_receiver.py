#!/usr/bin/env python3
from socket import *
import file_transfer as transfer
import sys


def get_ip_address():
    s = socket(AF_INET, SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()
    return ip

last = int()


def progress(x):
    stuff = last
    percent = int(x * 50)
    if percent <= stuff:
        return
    stuff = percent
    bar = ""
    for x in range(percent - 1):
        bar += "="
    bar += ">"
    for x in range(50 - percent):
        bar += " "
    print("\r[" + bar + "]", end="")
    sys.stdout.flush()


def read_file(sock, size):
    read = 0
    content = bytearray(size)
    while read < size:
        buf = sock.recv(size - read)
        content[read:read+len(buf)] = buf
        progress(read / size)
        read += len(buf)
    progress(1)
    print("")
    return content

sock = socket(AF_INET, SOCK_STREAM, proto=IPPROTO_TCP)
sock.setsockopt(SOL_SOCKET,SO_REUSEADDR,1)
sock.bind(("0.0.0.0", transfer.port))
sock.listen(20)
print("My IP is", get_ip_address())
try:
    while 1:
        client, addr = sock.accept()
        if client == None: raise Exception
        header = client.recv(transfer.fileheader_size)
        filename, size = transfer.dump_fileheader(header)
        print("Found file", filename, "with length", size, end="")
        input(" [enter]")
        transfer.begin_transfer(client)
        buf = read_file(client, size)
        with open(filename, "wb") as file_handle:
            file_handle.write(buf)
        transfer.complete_transfer(client)
        print("SUCCESS")
        sys.stdout.flush()
except KeyboardInterrupt:
    print("Cancelled")
    sock.close()
except Exception as ex:
    print(str(ex))
    sock.close()
