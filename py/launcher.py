import logging                 # Log
import socket                  # Main web connection creator
import os, sys                 # Get file name and stuff
import json_editor as json     # JSON interacting
import wx                      # Main GUI Lib
import file_transfer as trans  # Formatting data pack
from fnmatch import filter     # Check the number of existing logs
from threading import Thread   # Multi-threaded socket listening
from time import strftime      # Time for naming log file
from shutil import move        # Moving log files
from re import match           # Match blank strings


def cut_string(string, length):
    if len(string) > length:
        public_logger.debug('String "{}" cut.'.format(string))
        return string[:(length-1)] + '...'
    else:
        public_logger.debug('String "{}" haven\'t been cut.'.format(string))
        return string


def delete_logs():
    try:
        logs = filter(os.listdir('Logs/'), '*.log')
        public_logger.debug('Logs: '+str(logs))
        if len(logs) > 20:
            logs.sort()
            for log in logs[:len(logs)-20]:
                os.remove(os.path.join('Logs/', log))
                public_logger.debug('Removed outdated log file: '+os.path.join(
                    'Logs/', log))
            public_logger.info(str(len(logs)-20)+' logs are removed.')
    except FileNotFoundError:
        public_logger.warning('Log/ does not exist, creating...')
        os.mkdir('Logs/')


# Get the set logging level from file
def get_level():
    d = {'debug': logging.DEBUG,
         'info': logging.INFO,
         'warning': logging.WARNING,
         'error': logging.ERROR,
         'critical': logging.CRITICAL}
    f = open('debug')
    t = f.read()
    if t in d.keys(): return d[t]
    else: return logging.WARNING


# Get a filler to fill in the spaces in the GUI window
def get_filler(parent, length=20):
    public_logger.debug('StaticText filler with a length of {} was created.'
            .format(str(length)))
    return wx.StaticText(parent, label=' '*length)


def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()
    public_logger.info('Local IP address got: {}.'.format(ip))
    return ip


def get_ip_choices():
    str_list = []
    value_dict = {}
    for name, ip in json.get_ip_choices().items():
        str_list.append('{0} ({1})'.format(name, ip))
        value_dict[name] = ip
    choices = {'str':str_list, 'value':value_dict}
    return choices


# Logged decorator
def logged(cls):
    class_name = cls.__name__
    logger = logging.getLogger('P2P.'+class_name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(file_handler)
    logger.propagate = False
    logger.debug('Logger {} created.'.format(class_name))
    setattr(cls, 'logger', logger)
    return cls


def main():
    try:
        public_logger.info('Launching...')
        try:
            main_app = wx.App()
        except Exception as e:
            show_message('Unexpected error occured when creating main_app: {}'
                    .format(e), wx.ICON_ERROR)
            public_logger.critical(
                    'Unexpected error occured when creating main_app: {}'.format(e))
        launcher = MainFrame(None, title='P2P File Transfer')
        try:
            main_app.MainLoop()
        except Exception as e:
            show_message('Unexpected error occured: {}'.format(e), wx.ICON_ERROR)
            public_logger.critical('Unexpected error occured: {}'.format(e))
        public_logger.info('GUI closed.')
        delete_logs()

        file_handler.close()
        try:
            move('p2p_transfer.log', strftime('Logs/log_%y-%m-%d_%H-%M-%S.log'))
        except OSError as e:
            # As when multiple launchers were opened at the same time
            if e.winerror == 32:
                show_message("Another program is using log file. Now exiting.")
            else:
                show_message("Unexpected error when moving log file: {}".format(e), wx.ICON_ERROR)
    except Exception as e:
        public_logger.fatal('Unexpected error occured: {}'.format(e))
        show_message('We met a problem: {}. Please try to restart the program. Now exiting.'
                     .format(e)
                     )
    finally:
        json.close()
        sys.exit()


def show_message(message, style=wx.ICON_WARNING):
    dialog = wx.MessageDialog(None, message=message, style=style)
    return dialog.ShowModal()


# Settings saved in database
global_data = json.get_settings()

# Logging initiation
file_handler = logging.FileHandler("p2p_transfer.log")
file_handler.setLevel(get_level())  # To change the level, edit py/debug
formatter = logging.Formatter('[%(asctime)s] %(name)s (%(levelname)s): %(message)s')
file_handler.setFormatter(formatter)

# Public lgger (for functions and stuff)
public_logger = logging.getLogger('P2P')
public_logger.setLevel(logging.DEBUG)
public_logger.addHandler(file_handler)
public_logger.propagate = False


# Frame (window) classes
@logged
class MainFrame(wx.Frame):
    # I know grids might be simpler, but I'm more used to h/v sizers
    def __init__(self, *args, **kwargs):
        try:
            # Initiating
            self.logger.info('starting initialization of main frame...')
            wx.Frame.__init__(self, *args, **kwargs)
            self.file_name = ""
            self.file_content = None
            self.file_size = 0
            # If the list of IPs are updated by a "ViewAddrFrame", then
            self.ip_receiver = {'updated': False}
            # self.ip_receiver['updated'] will be True, and self.update_ip
            # will update the list of ip choices.
            self.target_ip = None
            self.recv_thread = None
            self.send_thread = None
            self.ip_choices = get_ip_choices()

            # Global containers
            main_vsizer = wx.BoxSizer(wx.VERTICAL)
            self.panel  = wx.Panel(self)

            # Send box
            send_box     = wx.StaticBox(self.panel, label='Send File')
            send_sizer   = wx.StaticBoxSizer(send_box)
            send_vsizer1 = wx.BoxSizer(wx.VERTICAL)  # "File:" and "IP:"
            send_vsizer2 = wx.BoxSizer(wx.VERTICAL)  # Filename and IP choosebox
            send_vsizer3 = wx.BoxSizer(wx.VERTICAL)  # Buttons
            self.logger.debug('Sendbox containers created.')

            file_label      = wx.StaticText(send_box, label='File: ')
            self.file_text  = wx.StaticText(send_box, label='[No File]\n')
            file_btn        = wx.Button(send_box, label='Browse...')
            ip_label        = wx.StaticText(send_box, label='\nIP: ')
            self.ip_chooser = wx.Choice(send_box, choices=
                    self.ip_choices['str'], size=(120, 50))
            # Filler for organizing widgets
            ip_filler       = get_filler(send_box, 6)
            # How you add ip
            ip_btn          = wx.Button(send_box, label='Edit...')
            # How you send file
            send_btn        = wx.Button(send_box, label='Send')
            self.logger.debug('Sendbox widgets created.')

            file_btn.       Bind(wx.EVT_BUTTON, self.on_choose_file)
            self.ip_chooser.Bind(wx.EVT_SET_CURSOR, self.update_ip)
            ip_btn.         Bind(wx.EVT_BUTTON, self.on_edit_ip)
            send_btn.       Bind(wx.EVT_BUTTON, self.on_send)
            self.logger.debug('Sendbox widgets bound.')

            send_vsizer1.Add(file_label)
            send_vsizer1.Add(ip_label)
            send_vsizer2.Add(self.file_text)
            send_vsizer2.Add(self.ip_chooser)
            send_vsizer3.Add(file_btn)
            send_vsizer3.Add(ip_btn)
            send_vsizer3.Add(send_btn)
            send_sizer.  Add(send_vsizer1)
            send_sizer.  Add(send_vsizer2)
            send_sizer.  Add(ip_filler)
            send_sizer.  Add(send_vsizer3)
            self.logger.debug('Sendbox widgets added onto containers.')
            self.logger.info('Sendbox created.')

            # Receive box
            recv_box     = wx.StaticBox(self.panel, label='Receive File',
                    size=(260, 200))
            recv_sizer   = wx.StaticBoxSizer(recv_box)
            recv_vsizer1 = wx.BoxSizer(wx.VERTICAL)
            self.logger.debug('Receivebox containers created.')

            my_ip_label     = wx.StaticText(recv_box,
                    label='My IP Address:                 ')
            self.my_ip_text = wx.StaticText(recv_box, label=get_ip_address())
            self.recv_btn   = wx.Button(recv_box, label='Start Listening')
            self.logger.debug('Receivebox widgets created.')

            self.recv_btn.Bind(wx.EVT_BUTTON, self.on_start_recv)
            self.logger.debug('Receivebox widgets bound.')

            recv_vsizer1.Add(my_ip_label)
            recv_vsizer1.Add(self.my_ip_text)
            recv_sizer.  Add(recv_vsizer1)
            recv_sizer.  Add(self.recv_btn)
            self.logger.debug('Receivebox widgets added onto containers.')
            self.logger.info('Receivebox created.')

            # Two buttons
            hsizer = wx.BoxSizer()

            set_btn  = wx.Button(self.panel, label='Setting...')
            quit_btn = wx.Button(self.panel, label='Quit')
            self.logger.debug('Bi-button created.')

            set_btn. Bind (wx.EVT_BUTTON, self.on_setting)
            quit_btn.Bind(wx.EVT_BUTTON, self.on_quit)
            self.logger.debug('Bi-button bound.')

            hsizer.Add(set_btn)
            hsizer.Add(get_filler(self.panel))
            hsizer.Add(quit_btn)
            self.logger.debug('Bi-button added onto sizer.')

            main_vsizer.Add(get_filler(self.panel))
            main_vsizer.Add(send_sizer, flag=wx.ALIGN_CENTER)
            main_vsizer.Add(get_filler(self.panel))
            main_vsizer.Add(recv_sizer, flag=wx.ALIGN_CENTER)
            main_vsizer.Add(get_filler(self.panel))
            main_vsizer.Add(hsizer, flag=wx.ALIGN_CENTER)
            self.logger.info('Main vertical sizer created.')
            self.panel.SetSizer(main_vsizer)
            self.SetSize((320, 310))
            self.Show()
            self.logger.info('Main frame initialized.')
        except OSError as e:
            if e.winerror == 10051:
                self.logger.critical('Cannot connect to internet correctly: {}'
                        .format(e))
                show_message('Are you connected to Internet?',wx.ICON_ERROR)
                self.on_quit(wx.EVT_BUTTON)
            else:
                self.logger.critical('Initiation failed: {}'.format(e))
                show_message('Error when initiating: {}'.format(e),
                        wx.ICON_ERROR)
                self.on_quit(wx.EVT_BUTTON)
        except Exception as e:
                self.logger.critical('Initiation failed: {}'.format(e))
                show_message('Error when initiating: {}'.format(e),
                        wx.ICON_ERROR)
                self.on_quit(wx.EVT_BUTTON)

    def update_ip(self, event):  # The user hovered over the ip chooser list
        self.logger.debug('IP chooser list box was hovered.')
        if self.ip_receiver['updated']:  # If user added a IP
            self.ip_choices = get_ip_choices()
            self.ip_chooser.Set(self.ip_choices['str'])
            self.logger.info('IP updated.')
        self.ip_receiver['updated'] = False

    def on_choose_file(self, event):
        # Basically copied from documentation
        with wx.FileDialog(self, "Choose file", wildcard="Any file (*.*)|*.*",
                style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:
            self.logger.info('File choosing dialog opened.')

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                self.logger.info('File choosing process cancelled.')
                return  # the user changed their mind

            # Proceed loading the file chosen by the user
            pathname = fileDialog.GetPath()
            self.file_name = os.path.basename(pathname)
            try:
                with open(pathname, 'rb') as file:
                    self.file_content = file.read()
                    self.file_size = len(self.file_content)
                    self.logger.info(
                            'Chosen file "{}" was read with a size of {}.'
                            .format(self.file_name, self.file_size))
            except IOError:
                show_message("Cannot open file '{name}'.".format(name=os.path.basename(pathname)))
                self.logger.warning('File {} cannot be opened.'
                        .format(os.path.basename(pathname)))
        file_label = cut_string(self.file_name, 10)
        self.file_text.SetLabel(file_label)
        self.logger.debug('Filename label updated to: {}'.format(file_label))

    def on_edit_ip(self, event):
        self.logger.debug('Edit... Button clicked.')
        editor = ViewAddrFrame(self.ip_receiver, self)
        editor.Show()

    def on_send(self, event):
        self.logger.debug('Send Button clicked.')
        # If there is a selection
        if self.ip_chooser.GetCurrentSelection() != -1:
            self.target_ip = list(self.ip_choices['value'].values()) \
                [self.ip_chooser.GetCurrentSelection()]
            self.logger.debug('Sending target IP set to: {}'
                    .format(self.target_ip))

        if self.target_ip is None:
            self.logger.warning('No target IP is chosen.')
            show_message(
                "No target ip is chosen, please choose the address to send.",
                wx.ICON_INFORMATION)
            return

        if (self.file_name is "") or (self.file_size is 0) or \
                (self.file_content is None):  # If no file is chosen
            self.logger.warning('No file is chosen.')
            show_message("No file is chosen, please choose the file to send.",
                    wx.ICON_INFORMATION)
            self.on_choose_file(event)
            return

        self.send_thread = SendThread(self.file_content, self.file_name,
                self.file_size, self.target_ip, self)
        self.logger.debug("Send thread {} created".format(self.send_thread))
        self.send_thread.start()
        self.logger.info("Send process (thread) started")

    def on_start_recv(self, event):
        self.logger.debug('Listen Button clicked.')
        self.recv_thread = RecvThread(self)
        self.logger.debug('Receive thread {} created.'.format(self.recv_thread))
        self.recv_thread.start()
        self.logger.info('Receiving process (thread) started.')
        self.recv_btn.SetLabel("Cancel")
        self.recv_btn.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.logger.debug('Receiving button changed.')

    def on_setting(self, event):
        self.logger.debug('Setting... Button clicked.')
        setter = SettingFrame(self)
        setter.Show()
        self.logger.debug('Setting frame {} shown.'.format(setter))

    def on_quit(self, event):
        self.logger.debug('Quit Button clicked.')
        if self.recv_thread:
            self.recv_thread.stop()
        self.logger.info('Ready to quit.')
        self.Destroy()

    def on_cancel(self, event):
        self.recv_thread.stop()
        self.recv_btn.SetLabel('Start Listening')
        self.recv_btn.Bind(wx.EVT_BUTTON, self.on_start_recv)
        self.logger.info('Listening stopped.')


@logged
class ProgressFrame(wx.Frame):
    def __init__(self, *args, **kwargs):
        self.logger.info('Starting initialization of receive frame {}...'
                .format(self))
        wx.Frame.__init__(self, *args, **kwargs)

        self.panel = wx.Panel(self)
        hsizer = wx.BoxSizer()
        vsizer = wx.BoxSizer(wx.VERTICAL)
        self.logger.debug('Containers created.')

        filler = wx.StaticText(self.panel, label='')
        self.loading_gauge = wx.Gauge(self.panel, size=(300, 20))
        progress_label = wx.StaticText(self.panel, label='    Status: ')
        self.progress_text = wx.StaticText(self.panel, label=' ')
        self.logger.debug('Widgets created.')

        hsizer.Add(progress_label)
        hsizer.Add(self.progress_text)
        self.logger.debug('Widgets added onto containers.')

        vsizer.Add(filler)
        vsizer.Add(self.loading_gauge, flag=wx.ALIGN_CENTER)
        vsizer.Add(hsizer)
        self.logger.debug('Main vsizer completed.')

        self.panel.SetSizer(vsizer)
        self.SetSize((380, 120))
        self.logger.info('Receive frame {} initialized.'.format(self))

    def set_text(self, text):
        self.progress_text.SetLabel(text)
        self.logger.debug('Progress text set to {}.'.format(text))

    def set_gauge_value(self, value):
        self.loading_gauge.SetValue(value)
        self.logger.debug('Gauge value set to {}'.format(value))

@logged
class SettingFrame(wx.Frame):
    def __init__(self, *args, **kwargs):
        self.logger.info('Starting initialization of setting frame {}...'
                .format(self))
        wx.Frame.__init__(self, *args, **kwargs)

        self.SetTitle("Settings - P2P File Transfer")

        self.changed_data = {'file_save_path': None}

        self.panel = wx.Panel(self)
        self.panel.SetSize(self.Size)
        main_vbox      = wx.BoxSizer(wx.VERTICAL)
        save_path_hbox = wx.BoxSizer()
        buttons_hbox   = wx.BoxSizer()
        self.logger.debug('Containers created.')

        self.save_path_label = wx.StaticText(self.panel,label='Save file in: '
                +cut_string(global_data['file_save_path'], 30)+' '*5)
        save_path_btn = wx.Button(self.panel, label='Browse...', size=(75, 27))
        cancel_btn  = wx.Button(self.panel, label='Cancel')
        confirm_btn = wx.Button(self.panel, label='Confirm')
        apply_btn   = wx.Button(self.panel, label='Apply')
        self.logger.debug('Save path widgets created.')
        self.logger.debug('Widgets created.')

        save_path_hbox.Add(self.save_path_label)
        save_path_hbox.Add(save_path_btn)
        buttons_hbox.  Add(cancel_btn)
        buttons_hbox.Add(apply_btn)
        buttons_hbox.  Add(confirm_btn)
        self.logger.debug('Widgets added onto containers.')

        save_path_btn.Bind(wx.EVT_BUTTON, self.on_choose_save_path)
        cancel_btn. Bind(wx.EVT_BUTTON, self.on_quit)
        confirm_btn.Bind(wx.EVT_BUTTON, self.on_confirm)
        apply_btn.  Bind(wx.EVT_BUTTON, self.on_apply)
        self.logger.debug('Widgets bound.')

        main_vbox.Add(save_path_hbox, flag=wx.ALIGN_CENTER)
        main_vbox.Add(buttons_hbox, flag=wx.ALIGN_CENTER)
        self.logger.debug('Main vsizer completed.')

        self.panel.SetSizer(main_vbox)
        self.SetSize((380, 100))
        self.SetMaxSize((380, 200))
        self.logger.info('Setting frame created.')

    def on_choose_save_path(self, event):
        self.logger.debug("Browse... Button clicked.")
        # Basically copied from documentation
        with wx.DirDialog(self, "Choose Download Directory",
                           global_data['file_save_path'],
                           style=wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST) as dirDialog:
            self.logger.info('Started choosing save path.')
            if dirDialog.ShowModal() == wx.ID_CANCEL:
                self.logger.info('Save path choosing process cancelled.')
                return  # the user changed their mind
            dirname = dirDialog.GetPath()
            self.logger.debug('Chosen save path {}'.format(dirname))
            self.changed_data['file_save_path'] = dirname
            self.save_path_label.SetLabel('Save file in: '
                    +cut_string(dirname, 30))
            self.logger.info('Saving path label changed to: {}.'
                    .format(dirname))

    def on_quit(self, event):
        self.logger.debug('Closing SettingFrame.')
        self.logger.info('Setting frame closed.')
        self.Destroy()

    def on_confirm(self, event):
        self.logger.debug('Confirm Button clicked.')
        self.on_apply(event)
        self.on_quit(event)

    def on_apply(self, event):
        self.logger.debug('Apply Button clicked.')
        for dataname in self.changed_data.keys():
            data = self.changed_data[dataname]
            if data:
                global_data[dataname] = data

        json.save_settings(global_data)
        self.logger.info('Setting data {} was saved to database.'
                .format(global_data))


@logged
class ViewAddrFrame(wx.Frame):
    def __init__(self, update_receiver, *args, **kwargs):
        self.logger.info("Starting initialization of IP edit frame")
        wx.Frame.__init__(self, *args, **kwargs)

        self.SetTitle("IP Edit - P2P File Transfer")

        # Attributes
        self.receiver = update_receiver
        self.ip_choices  = get_ip_choices()

        # Sizers
        self.panel = wx.Panel(self)
        whole_hsizer = wx.BoxSizer()
        left_vsizer  = wx.BoxSizer(wx.VERTICAL)
        right_vsizer = wx.BoxSizer(wx.VERTICAL)

        self.listbox = wx.ListBox(self.panel, style=wx.LB_SORT|wx.LB_NEEDED_SB,
                size=(200, 232),choices=self.ip_choices['str'])
        edit_btn     = wx.Button(self.panel, label='Edit...')
        add_btn      = wx.Button(self.panel, label='Add...')
        del_btn      = wx.Button(self.panel, label='Delete')

        self.        Bind(wx.EVT_CLOSE, self.on_close)
        self.listbox.Bind(wx.EVT_LISTBOX_DCLICK, self.on_edit)
        self.listbox.Bind(wx.EVT_SET_CURSOR, self.on_update)
        edit_btn.    Bind(wx.EVT_BUTTON, self.on_edit)
        add_btn.     Bind(wx.EVT_BUTTON, self.on_add)
        del_btn.     Bind(wx.EVT_BUTTON, self.on_delete)

        right_vsizer.Add(get_filler(self.panel, 10))
        right_vsizer.Add(edit_btn)
        right_vsizer.Add(get_filler(self.panel, 10))
        right_vsizer.Add(add_btn)
        right_vsizer.Add(get_filler(self.panel, 10))
        right_vsizer.Add(del_btn)
        left_vsizer. Add(get_filler(self.panel, 5))
        left_vsizer. Add(self.listbox)
        whole_hsizer.Add(get_filler(self.panel, 10))
        whole_hsizer.Add(left_vsizer)
        whole_hsizer.Add(get_filler(self.panel, 10))
        whole_hsizer.Add(right_vsizer)

        self.panel.SetSizer(whole_hsizer)
        self.SetSize((400, 310))

    def on_edit(self, e):
        self.logger.debug(
                'Edit Button clicked or a selection is doubleclicked.')
        try:
            name_selected = list(self.ip_choices['value'].keys()) \
                [self.listbox.GetSelection()]
            editor = EditAddrFrame(self.receiver, False,
                                   json.get_ip_data(name_selected), parent=self)
            editor.Show()
        except TypeError as e:
            self.logger.warning('TypeError occured when editing IP: no IP chosen.')
        except IndexError:
            pass  # There is no addresses stored.

    def on_add(self, e):
        self.logger.debug('Add Button clicked.')
        adder = EditAddrFrame(self.receiver, True, parent=self)
        adder.Show()

    def on_delete(self, e):
        self.logger.debug('Delete Button clicked.')
        try:
            ip_name_removing = list(self.ip_choices['value'].keys()) \
                [self.listbox.GetSelection()-1]
            json.delete_ip(ip_name_removing)
        except TypeError as e:
            self.logger.warning('TypeError occured when deleting IP: no IP chosen.')
            pass
        self.receiver['updated'] = True
        self.on_update(e)

    def on_update(self, e):
        if self.receiver['updated']:
            self.ip_choices = get_ip_choices()
            self.listbox.Set(self.ip_choices['str'])
            self.receiver['updated'] = False
            self.logger.debug("Listbox updated to {}"
                    .format(self.ip_choices['str']))

    def on_close(self, e):
        self.receiver['updated'] = True
        self.Destroy()


@logged
class EditAddrFrame(wx.Frame):
    def __init__(self, finish_receiver, isadd, original_data=('', ''),
            *args, **kwargs):
        self.logger.info('Starting initialization of address adding frame {}...'
                .format(self))
        wx.Frame.__init__(self, *args, **kwargs)
        self.SetTitle('Add Address - P2P File Transfer')

        # Attribute setups
        self.receiver = finish_receiver
        self.is_add   = bool(isadd)
        self.original_ip, self.original_name = original_data

        # Create containers
        self.panel   = wx.Panel(self)
        vsizer       = wx.BoxSizer(wx.VERTICAL)
        up_hsizer    = wx.BoxSizer()
        down_hsizer  = wx.BoxSizer()
        left_vsizer  = wx.BoxSizer(wx.VERTICAL)
        right_vsizer = wx.BoxSizer(wx.VERTICAL)
        self.logger.debug('Containers created.')

        # Create widgets
        text1 = wx.StaticText(self.panel, label='  Name: \n', size=(150, 20))
        text2 = wx.StaticText(self.panel, label='  IP Address:  \n',
                size=(150, 20))
        self.field_name = wx.TextCtrl(self.panel, value=self.original_name,
                size=(170, 20), style=wx.TE_PROCESS_ENTER)
        self.field_ip = wx.TextCtrl(self.panel, value=self.original_ip,
                size=(170, 20), style=wx.TE_PROCESS_ENTER)
        confirm_btn = wx.Button(self.panel, label='Confirm')
        quit_btn = wx.Button(self.panel, label='Close')
        self.logger.debug('Widgets created.')

        # Bind
        self.field_name.Bind(wx.EVT_TEXT_ENTER, self.on_confirm)
        self.field_ip.  Bind(wx.EVT_TEXT_ENTER, self.on_confirm)
        confirm_btn.    Bind(wx.EVT_BUTTON, self.on_confirm)
        quit_btn.       Bind(wx.EVT_BUTTON, self.on_quit)
        self.logger.debug('Widget bound.')

        # Add widgets into containers
        left_vsizer. Add(text1)
        left_vsizer. Add(get_filler(self.panel))
        left_vsizer. Add(text2)
        right_vsizer.Add(self.field_name)
        right_vsizer.Add(get_filler(self.panel))
        right_vsizer.Add(self.field_ip)
        up_hsizer.   Add(left_vsizer)
        up_hsizer.   Add(right_vsizer)
        down_hsizer. Add(quit_btn)
        down_hsizer. Add(get_filler(self.panel, 50))
        down_hsizer. Add(confirm_btn)
        vsizer.      Add(get_filler(self.panel))
        vsizer.      Add(up_hsizer, flag=wx.ALIGN_CENTER)
        vsizer.      Add(get_filler(self.panel))
        vsizer.      Add(down_hsizer, flag=wx.ALIGN_CENTER)
        vsizer.Add(get_filler(self.panel))
        self.logger.debug('Widgets added onto containers.')

        # Set frame
        self.panel.SetSizer(vsizer)
        self.SetSize((350, 170))
        self.logger.debug('Address adding frame {} created.'.format(self))

    def on_confirm(self, e):
        # Get data from TextCtrl
        self.logger.info('IP Adding confirmation process started.')
        name = self.field_name.GetLineText(0)
        ip = self.field_ip.GetLineText(0)
        self.logger.debug('IP "{}" named "{}" is got.'
                          .format(ip, name))

        # Check if IP or name is legal
        split = ip.split('.')
        if not name or not ip:
            self.logger.warning('A field is left blank')
            show_message('Please fill in every field.')
            return False

        if len(split) != 4:  # If it isn't a form of x.x.x.x
            self.logger.warning('IP {} is not legal.'.format(ip))
            show_message("{} is not a valid IP address".format(ip))
            return False

        if match('^[\s ]*$', name):  # If it is blank or only has spaces
            self.logger.warning('IP name is not entered.')
            show_message('Please enter a name')
            return False

        if name in ('False', 'True', 'None'):
            self.logger.warning('Name {} is illegal.'.format(name))
            show_message('Illegal IP name, choose a better one!')
            return False

        if self.is_add:
            if json.name_exists(name):
                self.logger.warning('Name {} exists.'.format(name))
                show_message('This name is taken, use another one')
                return False
            json.add_ip(name, ip)
            self.receiver['updated'] = True
            show_message('Successfully adding IP {}'.format(ip), wx.ICON_INFORMATION)
        else:  # is edit
            if self.original_ip == ip and self.original_name == name:
                self.logger.warning('User did not change both name and IP.')
                show_message('Please do some edits. Otherwise, click the "Close" button.')
                return False
            elif json.name_exists(name):
                self.logger.warning('Name {} exists.'.format(name))
                show_message('This name is taken, use another one')
                return False
            else:
                json.edit_ip(self.original_name, name, ip)
                self.receiver['updated'] = True

        self.field_name.SetValue('')
        self.field_ip.SetValue('')
        self.on_quit(e)

    def on_quit(self, e):
        self.Destroy()


# Receiving and sending Threading classes
@logged
class RecvThread(Thread):
    def __init__(self, frame, *args, **kwargs):
        Thread.__init__(self, *args, **kwargs)
        self.frame = frame
        self.client = None
        self.addr = None
        self.sock = None

    def run(self):
        self.logger.debug('Receive thread {} run.'.format(self))
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM,
                proto=socket.IPPROTO_TCP)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(("0.0.0.0", 2333))
        self.logger.debug('Socket listening started.')
        self.sock.listen(20)
        try:
            client, addr = self.sock.accept()
            self.client, self.addr = client, addr
            self.recv_file()
        except OSError:
            return

    def stop(self):
        self.sock.close()
        self.logger.debug('Receive thread {} stopped.'.format(self))

    def recv_file(self):
        try:
            header = self.client.recv(trans.fileheader_size)
            filename, size = trans.dump_fileheader(header)
            y_o_n = show_message('Found file "{name}" with length {size}, receive?'
                    .format(name=filename, size=str(size)),wx.YES_NO | wx.ICON_QUESTION)
            self.logger.debug('Found file "{}" with length {}.'
                    .format(filename, size))
            progress_window = ProgressFrame(self.frame,
                    title='Receiving {} - P2P File Transfer'.format(filename))
            progress_window.Show()
            self.logger.debug('Process window {} shown.'.format(progress_window))
            if y_o_n == wx.ID_YES:
                self.logger.info('Start receiving file "{}" size {}.'
                        .format(filename, size))
                trans.begin_transfer(self.client)
                read = 0
                content = bytearray(size)
                progress_window.set_text("Receiving")
                while read < size:
                    buf = self.client.recv(size - read)
                    content[read:read + len(buf)] = buf
                    #progress_window.set_gauge_value(read / size)
                    read += len(buf)
                self.logger.debug('All file information received.')
                progress_window.set_gauge_value(100)

                progress_window.set_text("Writing")
                if os.path.isfile(os.path.join(global_data['file_save_path'], filename)):
                    new_filename = strftime('p2p_%H-%M-%S_{}'.format(filename))
                else:
                    new_filename = filename
                path = os.path.join(global_data['file_save_path'], new_filename)
                with open(path, 'wb') as file:
                    file.write(content)
                trans.complete_transfer(self.client)
                progress_window.set_text("Finished")
                self.logger.info('Finished receiving {}. Saved in: {}'.format(filename, path))
                show_message('Finished receiving file: {}. Saved in: {}'.
                    format(filename, path),
                    wx.ICON_INFORMATION
                )
                progress_window.Destroy()
                self.frame.recv_btn.SetLabel('Start Listening')
                self.frame.recv_btn.Bind(wx.EVT_BUTTON,
                        self.frame.on_start_recv)
                self.logger.debug('Receive button changed to init.')

            elif y_o_n == wx.ID_NO:
                self.logger.debug('File receving denied.')
                progress_window.set_text("Cancelled")
                self.client.send(b'No me gustas')
                progress_window.Destroy()
                self.sock.close()
                self.frame.recv_btn.SetLabel('Start Listening')
                self.frame.recv_btn.Bind(wx.EVT_BUTTON,
                        self.frame.on_start_recv)
                self.logger.debug('Receive button changed to init.')
        except Exception as e:
            self.logger.error('Unexpected error while receiving file: {}'
                    .format(e))
            show_message('Unexpected error while receiving file: {}'.format(e),
                    wx.ICON_ERROR)


@logged
class SendThread(Thread):
    def __init__(self, file, name, size, ip, frame, *args, **kwargs):
        Thread.__init__(self, *args, **kwargs)
        self.file_content = file
        self.file_name = name
        self.file_size = size
        self.target_ip = ip
        self.parent_frame = frame
        self.header = None
        self.sock = None

    def run(self):
        try:
            self.logger.debug("Send thread {} run.".format(self))
            self.header = trans.fill_fileheader(self.file_name, self.file_size)
            self.sock = socket.socket()
            self.logger.debug("Sending socket set up.")
            try:
                self.logger.info("Sending {} to {}.".format(self.file_name,
                    self.target_ip))
                progress_window = ProgressFrame(self.parent_frame,
                        title="Sending {} to {} - P2P File Transfer"
                        .format(self.file_name, self.target_ip))
                progress_window.Show()
                progress_window.set_text("Sending")
                try:
                    self.sock.connect((self.target_ip, trans.port))
                except OSError as e:
                    self.logger.warning("Connection refused by the client")
                    raise ConnectionRefusedError
                if not trans.check_server(self.sock, self.header) or \
                        not trans.check_accepted:
                    raise ConnectionRefusedError

                self.sock.send(self.file_content)
                if trans.check_complete(self.sock):
                    progress_window.set_gauge_value(100)
                    show_message("Sending progress successfully completed.",
                            wx.ICON_INFORMATION)
                    self.logger.info("Sending completed")
                    progress_window.Destroy()
                    return
                else:
                    raise ConnectionError
            except ConnectionRefusedError:
                show_message("{} is not listening."
                        .format(self.target_ip), wx.ICON_INFORMATION)
                self.logger.warning("Connection refused by {}.".format(self.target_ip))
                progress_window.set_gauge_value(0)
                progress_window.Destroy()
                return
            except ConnectionError:
                show_message("Transmission incomplete, check your connection!")
                self.logger.warning('Sending process to {} is incomplete.'
                        .format(self.target_ip))
                progress_window.set_gauge_value(0)
                progress_window.Destroy()
                return
            except OSError as e:
                if e.winerror == 10051:
                    show_message("Are you connected to the Internet?",
                                 wx.ICON_ERROR)
                    self.logger.error('The internet is unreachable: {}'
                            .format(e))
                else:
                    show_message("Unexpected error while sending file: {}"
                            .format(e), wx.ICON_ERROR)
                    self.logger.error("Unexpected error while sending file: {}"
                            .format(e))
            except Exception as e:  # Other exceptions
                show_message("Unexpected exception when sending file: {}"
                        .format(str(e)), wx.ICON_ERROR)
                self.logger.warning('Unexpected exception during sending: {}'
                        .format(e))
                progress_window.loading_gauge.SetValue(0)
                progress_window.Destroy()
                return
            finally:
                self.sock.close()
                self.logger.info('Sending process ended.')
        except OSError:
            return

    def stop(self):
        self.sock.close()
        self.logger.debug("Send thread {} stopped.".format(self))


if __name__ == '__main__':
    main()
