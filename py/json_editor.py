import json
import logging
import sys

JSON_FILE = '../p2p_data.json'
current_data = None
logger = logging.getLogger('P2P.JsonEditor')
logger.setLevel(logging.DEBUG)


def test():
    print(load_json())


def load_json():
    global current_data
    try:
        current_data = json.load(open(JSON_FILE))
    except json.decoder.JSONDecodeError as decodeError:
        current_data = {'Addresses': {'localhost': '127.0.0.1'}, 'Settings': {'file_save_path':'..'}}
        write_json(current_data)
    return current_data


def write_json(data: dict):
    if not data:
        load_json()
    try:
        f = open(JSON_FILE, 'w')
        f.write(json.dumps(data))
        f.close()
    except IOError as e:
        logger.error('IOError when writing JSON: {}'.format(e))
    finally:
        current_data = data


def get_ip_choices():
    return load_json()['Addresses']


def get_ip_data(name):
    if name_exists(name):
        found = current_data['Addresses'][name]
    else:
        found = ''
    logger.debug("{} is found for name query {}.".format(found, name))
    return (found, name)


def name_exists(name):
    if name in current_data['Addresses'].keys():
        logger.debug('{} is in Addresses.'.format(name))
        return True
    logger.debug('{} is not in Addresses.'.format(name))
    return False


def add_ip(name, ip):
    current_data['Addresses'][name] = ip
    write_json(current_data)
    logger.info('IP {}({}) added into JSON.'.format(ip, name))


def delete_ip(name):
    if name_exists(name):
        del current_data['Addresses'][name]
        write_json(current_data)
        logger.info('IP {} deleted from Addresses.'.format(name))
    else:
        logger.info('IP {} is not found in JSON.'.format(name))


def edit_ip(original_name, name, ip):
    delete_ip(original_name)
    add_ip(name, ip)


def get_settings():
    return load_json()['Settings']


def save_settings(settings: dict):
    if len(settings.keys()) == len(current_data['Settings'].keys()):
        current_data['Settings'] = settings
        write_json(current_data)
        logger.info('Settings {} saved into database.'.format(settings))
    else:
        logger.info('Settings {} do not match format, so they are not saved.'.format(settings))


def close():
    sys.exit()


if __name__ == '__main__':
    test()
