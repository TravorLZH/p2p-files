import sys
import os
import json_editor as json
import logging
import socket


def cut_string(string, length):
    if len(string) > length:
        public_logger.debug('String "{}" cut.'.format(string))
        return string[:(length-1)] + '...'
    else:
        public_logger.debug('String "{}" haven\'t been cut.'.format(string))
        return string


def delete_logs():
    try:
        logs = filter(os.listdir('Logs/'), '*.log')
        public_logger.debug('Logs: '+str(logs))
        if len(logs) > 20:
            logs.sort()
            for log in logs[:len(logs)-20]:
                os.remove(os.path.join('Logs/', log))
                public_logger.debug('Removed outdated log file: '+os.path.join(
                    'Logs/', log))
            public_logger.info(str(len(logs)-20)+' logs are removed.')
    except FileNotFoundError:
        print('Log/ does not exist, creating...')
        os.mkdir('Logs/')


# Get the set logging level from file
def get_level():
    d = {'debug': logging.DEBUG,
         'info': logging.INFO,
         'warning': logging.WARNING,
         'error': logging.ERROR,
         'critical': logging.CRITICAL}
    f = open('debug')
    t = f.read()
    if t in d.keys(): return d[t]
    else: return logging.WARNING


# Get a filler to fill in the spaces in the GUI window
def get_filler(parent, length=20):
    public_logger.debug('StaticText filler with a length of {} was created.'
            .format(str(length)))
    return wx.StaticText(parent, label=' '*length)


def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()
    public_logger.info('Local IP address got: {}.'.format(ip))
    return ip


def get_ip_choices():
    str_list = []
    value_dict = {}
    for name, ip in json.get_ip_choices().items():
        str_list.append('%s (%s)' % (name, ip))
        value_dict[name] = ip
    choices = {'str':str_list, 'value':value_dict}
    return choices


# Logged decorator
def logged(cls):
    class_name = cls.__name__
    logger = logging.getLogger('P2P.'+class_name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(file_handler)
    logger.propagate = False
    logger.debug('Logger {} created.'.format(class_name))
    setattr(cls, 'logger', logger)
    return cls


# Settings saved in database
global_data = json.get_settings()

# Logging initiation
file_handler = logging.FileHandler("p2p_transfer.log")
file_handler.setLevel(get_level())  # To change the level, edit py/debug
formatter = logging.Formatter('[%(asctime)s] %(name)s (%(levelname)s): %(message)s')
file_handler.setFormatter(formatter)

# Public lgger (for functions and stuff)
public_logger = logging.getLogger('P2P')
public_logger.setLevel(logging.DEBUG)
public_logger.addHandler(file_handler)
public_logger.propagate = False


def main():

