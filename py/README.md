Python implementation of P2P File Transfer
==========================================

This was originally lauched as an experiment to do networking in Python, but
gradually this became the frequently maintained part in this project.

You may run these Python programs in any systems having `python3` installed,
but for enabling GUI, you must install `wxPython`.

After completing major elements, an EXE will be updated.

## Advantages for using PFT in Python

* You would not worry about portability because Python already helps you fix
them. That is why this software can be shared conveniently.

* Python programs can be easily written and extended, so your suggestion on this
project can be immediately applied.
