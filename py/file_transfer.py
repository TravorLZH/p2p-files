import struct

# Size of `struct fileheader` equal 128 bytes for filename
# plus 4 bytes for file size, which is 132 bytes total
fileheader_size = 132

# The response from a file receiver meaning the transfer
# request is accepted
proper_response = "Que tal"

# The response from a file receiver meaning the transfer
# request is completed
complete_response = "Adios"

# This port is used to transfer files
port = 2333


def check_server(sock, header):
    sock.send(header)
    buf = sock.recv(32).decode()
    return buf.split('\0')[0] == proper_response


def begin_transfer(sock):
    buf = proper_response
    buf += '\0'
    sock.send(buf.encode())


def fill_fileheader(filename, size):
    return struct.pack("128s", filename.encode("utf-8"))+struct.pack("!I", size)


def dump_fileheader(header):
    return (header[:128].decode().split('\0')[0],
            struct.unpack(">I", header[128:])[0])


def complete_transfer(sock):
    buf = complete_response
    buf += '\0'
    sock.send(buf.encode())


def check_complete(sock):
    buf = sock.recv(32).decode()
    return buf.split('\0')[0] == complete_response


def check_accepted(sock):
    resp = sock.recv(32).decode()
    return resp.split('\0')[0] == proper_response
