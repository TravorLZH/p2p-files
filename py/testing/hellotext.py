import wx

class HelloFrame(wx.Frame):
    def __init__(self,*args,**kw):
        super(HelloFrame,self).__init__(*args,**kw)
        # Create panel
        panel=wx.Panel(self)
        st=wx.StaticText(panel,label="Hello world",pos=(0,0))
        font=st.GetFont()
        font.PointSize+=10
        font=font.Bold()
        st.SetFont(font)

app=wx.App()
win=HelloFrame(None,title="Better Hello")
win.Show()
app.MainLoop()
