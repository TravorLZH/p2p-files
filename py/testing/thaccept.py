import wx
from socket import *
from threading import Thread

sock=socket(AF_INET,SOCK_STREAM,proto=IPPROTO_TCP)
sock.setsockopt(SOL_SOCKET,SO_REUSEADDR,1)
sock.bind(("0.0.0.0",2333))
sock.listen(20)

class AcceptThread(Thread):
    def run(self):
        try:
            client,addr=sock.accept()
            client.close()
        except Exception: # EBADF or WinError 10038
            return
        frm.SetStatusText("Accepted "+addr[0])

class MainFrame(wx.Frame):
    def __init__(self,*args,**kw):
        super(MainFrame,self).__init__(*args,**kw)
        self.CreateStatusBar()
        self.SetStatusText("This will change")
        self.Show()
        background=AcceptThread()
        background.start()
        self.Bind(wx.EVT_CLOSE,self.on_close)

    def on_close(self,event):
        sock.close()
        self.Destroy()

app=wx.App(wx.Frame)
frm=MainFrame(None,title="Waiting for request")
app.MainLoop()
