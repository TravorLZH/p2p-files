import wx
import time
import threading
from __init__ import say


class Progress(wx.ProgressDialog):
    def __init__(self, *args, **kwargs):
        wx.ProgressDialog.__init__(self, *args, **kwargs)
        self.SetRange(100)
        self.Update(1)
        say("progress dialog successfully created")


class CancelFrame(wx.Frame):
    def __init__(self, *args, **kwargs):
        wx.Frame.__init__(self, *args, **kwargs)
        self.child = None
        cancel_btn = wx.Button(self, label='cancel', pos=(0,0))

    def set_child(self, child):
        self.child = child

    def on_cancel(self):
        self.child.Destroy()
        self.Destroy()


class LoadingThread(threading.Thread):
    def __init__(self, id, name, start_num=0):
        threading.Thread.__init__(self)
        self.id = id
        self.name = name
        self.start_num = start_num

    def run(self):
        v = self.start_num
        while True:
            progs.Update(v)
            v += 1
            if v >= 100:
                v = 99
                progs.Update(v)
                break
            time.sleep(2)

app = wx.App()
can = CancelFrame(None, title='cancel frame')
progs = Progress('test progress dialog', 'hello progress!')
can.set_child(progs)
progs.Show()
can.Show()
app.MainLoop()
thread1 = LoadingThread(0, 'loading-1')
thread1.start()
thread1.join()



