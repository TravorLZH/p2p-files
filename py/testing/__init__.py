import sys


def say(*args):
    sys.stdout.write(" ".join(map(str, args)) + "\n")
