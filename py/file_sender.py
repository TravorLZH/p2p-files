#!/usr/bin/env python3
from socket import *
#import socketserver
import os
import sys
import file_transfer as ft

if len(sys.argv)<3:
    print("Usage:",sys.argv[0],"[file] [ip address]")
    quit(1)

filename=os.path.basename(sys.argv[1])
targetip=sys.argv[2]

with open(sys.argv[1],"rb") as file_handle:
    file_content=file_handle.read()
    file_size=len(file_content)

print("Sending file",filename,"to",targetip)
header=ft.fill_fileheader(filename,file_size)
sock=socket()
try:
    sock.connect((targetip,ft.port))
    if ft.check_server(sock,header)==False:
        raise ConnectionRefusedError
    # Send the file
    sock.send(file_content)
    ft.check_complete(sock)
except ConnectionRefusedError:
    print(targetip+": Not a file receiver")
sock.close()
