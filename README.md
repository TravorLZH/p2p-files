P2P File Transfer
=================
This project helps me learn networking. It's a tool to send files between two
machines that uses the same Wi-Fi.

## Installation

For Linux users, make sure that you have [GNU Make][1] installed, so you can
install the programs using the following steps:
```shell
$ make -C linux
# make install -C linux DEST=/path/to/dest
```

## Transfer Protocol

This project uses P2P File Transfer Protocol (Not to be confused with FTP).
My implementation of the protocol behaves like the following:

* Receiver listens at **0.0.0.0:2333**
* Sender connects and sends `fileheader` to server as a request
* Receiver sends `Que tal` as a response to accept the request
* Receiver now sends file content to the server.
* Receiver writes the read content to local file
* Receiver sends `Adios` to sender to complete the transfer
* Sender closes the connection

In which the file header's prototype is
```c
struct fileheader {
	char filename[128];	// File name (not path)
	unsigned int size;	// File length
};
```

> NOTE: This protocol is based on TCP (a connection based protocol) instead
of UDP

## Receive files

You can run the command `filereceiver` in the directory where you want to save
the files. For example
```shell
$ cd ~/code
$ filereceiver
My IP is 192.168.1.109
Waiting for request
Received file main.c with length 421
Saving file to ./main.c
main.c: SUCCESS
```

## Send files

You must have `filereceiver` running in the other computer before you do the
following
```shell
$ filesender main.c 192.168.1.109
Sending main.c to 192.168.1.109
main.c: SUCCESS
```

## Limits

In order to support 32 bit systems, I use a 32 bit unsigned integer to store
the size of the file which means you cannot send file that exceeds 4 GB. You
may refer to commit 01c0bace.

[1]: https://www.gnu.org/software/make
